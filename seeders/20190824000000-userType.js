'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('userTypes', [
          {
              name: 'user'
          },
          {
              name: 'admin'
          },
          {
              name: 'super admin'
          }
      ])
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('userTypes', null, {})
  }
};
