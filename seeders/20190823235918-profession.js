'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.bulkInsert('professions', [
          {
              name: 'Software development'
          },
          {
              name: 'Marketing'
          },
          {
              name: 'Accounting'
          },
          {
              name: 'Business intelligence'
          },
          {
              name: 'Business development'
          },
          {
              name: 'Human resources'
          },
          {
              name: 'Finance'
          },
          {
              name: 'Other'
          }
      ])
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('professions', null, {})
  }
};
