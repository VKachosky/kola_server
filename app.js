var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var domaine = '/api/auth/';
const key = 'kol@Key1345';
var jwt = require('jsonwebtoken');
var cors = require('cors');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors());

app.use(function (req, res, next){
    const token = req.headers.token; // || req.query.token || req.headers['x-access-token']
    // decode token
    if(!(req.originalUrl ==  domaine + 'login' ||req.originalUrl == domaine + 'register' || req.originalUrl == '/api/professions') ){
        if (token) {
            // verifies secret and checks exp
            jwt.verify(token,key, function(err, decoded) {
                if (err) {
                    return res.send({status: 0, error: 'Unauthorized access.' });
                }
                else{
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            // if there is no token
            // return an error
            return res.send({
                status: 0,
                error: 'No token provided.'
            });
        }
    }
    else{
        next();
    }

});
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
