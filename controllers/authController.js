var user = require('../models').Users;
var userType = require('../models').UserType;
var jwt = require('jsonwebtoken');

const key = 'kol@Key1345';

module.exports = {

    login(req, res){
        user.findOne({
            include:{
                model: userType,
                as: 'UserType'
            },
            where: {
                email: req.body.email
            }
        })
            .then(_user => {
                if(!_user){
                    return res.send({
                        status: 1,
                        message: 'Authentication failed. User not existed in app'
                    })
                }
                _user.comparePassword(req.body.password, (err, isMatch) => {
                    if(isMatch && !err){
                        var token = jwt.sign(JSON.parse(JSON.stringify(_user)), key, {expiresIn: '876000h'})
                        res.send({
                            status: 1,
                            data: {
                                token: '' + token,
                                id: _user.dataValues.id,
                                user_type: (_user.dataValues.UserType.name == 'user')? 'Team member': 'Team leader',
                                message: "welcome " + _user.dataValues.name
                            }
                        })
                    }
                    else {
                        res.send({
                            status: 1,
                            message: 'Authentication failed. incorrect email or password.'
                        })
                    }
                })
            })
            .catch(error => res.send({
                status: 0,
                error: error.message
            }))
    },

    register(req, res){

        user.findOne({
            where: {
                email: req.body.email
            }
        })
            .then(_user => {
                if(_user){
                    res.send({
                        status: 1,
                        message: 'Email used for another user in the app'
                    })
                }
                else{
                    user
                        .create({
                            name: req.body.name,
                            email: req.body.email,
                            password: req.body.password,
                            userType_id: (req.body.user_type == 'Team member')? 1: 2,
                            profession_id: req.body.profession_id
                        })
                        .then(userCreated => {
                            var token = jwt.sign(JSON.parse(JSON.stringify(userCreated)), key, {expiresIn: '876000h'})
                            res.send({
                                status: 1,
                                data: {
                                    token: '' + token,
                                    id: userCreated.dataValues.id,
                                    message: 'User register successful',
                                    user_type: req.body.user_type
                                }
                            })
                        })
                        .catch(error => {
                            res.send({
                                status: 0,
                                error: error.message
                            })
                        })
                }
            })
            .catch(error => {
                res.send({
                    status: 0,
                    error: error.message
                })
            })
    }
}