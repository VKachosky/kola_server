var user = require('../models').Users;
var task = require('../models').Task;
var team = require('../models').Team;
var profession = require('../models').Profession;
var userType = require('../models').UserType;
var team_member_task = require('../models').Team_member_task;
var team_member = require('../models').Team_member;
var Sequelize = require('sequelize');


module.exports = {

    list_users(req, res) {
        user
            .findAll({
                where: {
                    id: {
                        [Sequelize.Op.ne]: req.body.user_id
                    }
                },
                attributes: ['id','email', 'name']
            })
            .then(users => {
                res.send({
                    status: 1,
                    data:  {
                        users: users
                    }
                })
            })
    },

    list(req, res){
        var idTeams = new Array();
        if(!req.body.user_id){
            res.send({
                status: 1,
                message: 'User not existed in app'
            })
        }
        else {
            team_member
                .findAll({
                    where: {
                        user_id: req.body.user_id
                    }
                })
                .then(team_members => {
                    if(team_members.length === 0){
                        res.send({
                            status: 1,
                            data: {
                                users: []
                            }
                        })
                    }
                    else {
                        team_members.forEach(item => {
                            idTeams.push(item.dataValues.team_id)
                        })
                    }
                })
                .then(() => {
                    team_member
                        .findAll({
                            include: [
                                {
                                    model: team,
                                    as: 'Team'
                                },
                                {
                                    model: user,
                                    as: 'User'
                                }
                            ],
                            where: {
                                team_id: {
                                    [Sequelize.Op.in]: idTeams
                                }
                            }
                        })
                        .then(values => {
                            res.send({
                                status: 1,
                                data: {
                                    users: values
                                }

                            })
                        })
                        .catch(error => {
                            res.send({
                                status: 0,
                                error: error.message
                            })
                        })
                })
                .catch(error => {
                    res.send({
                        status: 0,
                        error: error.message
                    })
                })
        }

    },

    editProfil(req,res){
        if(!req.body.user_id){
            res.send({
                status: 1,
                message: 'user not existed'
            })
        }
        else {
            user
                .findByPk(req.body.user_id)
                .then(_user => {
                    if(!_user){
                        res.send({
                            status: 1,
                            message: 'user not found'
                        })
                    }
                    else{
                        _user
                            .update({
                                name: req.body.name,
                                password: req.body.password,
                                email: req.body.email
                            })
                            .then(() => {
                                res.send({
                                    status: 1
                                })
                            })
                            .catch(error => {
                                res.send({
                                    status: 0,
                                    error: error.message
                                })
                            })
                    }
                })
                .catch(error => {
                    res.send({
                        status: 0,
                        error: error.message
                    })
                })
        }
    },

    getUserInformation(req, res){
        if(!req.body.user_id){
            res.send({
                status: 1,
                message: 'User not existed'
            })
        }
        else{
            user
                .findOne({
                    include: [
                        {
                            model: profession,
                            as: 'Profession'
                        },
                        {
                            model: userType,
                            as: 'UserType'
                        }
                    ],
                    where: {
                        id: req.body.user_id
                    }
                })
                .then(_user => {
                    res.send({
                        status: 1,
                        data: {
                            user: {
                                name: _user.dataValues.name,
                                email: _user.dataValues.email,
                                profession: _user.dataValues.Profession.name,
                                user_type: (_user.dataValues.UserType.name == 'user')? 'Team member': 'Team leader'
                            }
                        }
                    })
                })
                .catch(error => {
                    res.send({
                        status: 0,
                        error: error.message
                    })
                })
        }
    }
}