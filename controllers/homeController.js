var user = require('../models').Users;
var task = require('../models').Task;
var profession = require('../models').Profession;
var team_member_task = require('../models').Team_member_task;
var team_member = require('../models').Team_member;
var Sequelize = require('sequelize');
var moment = require('moment');


module.exports = {

    index(req, res){
        var doneTasks = 0;
        var taskNumber = 0;
        var ongoingTasks = 0;
        var blockedTasks = 0;
        var idTasks = new Array();
        var idTeams = new Array();
        var idTeamTasks = new Array();
        var nIdTeams = new Array()
        var now = moment(new Date()).format('YYYY-MM-DD');
        var nextDay = (moment(now).add(1, 'days')).format('YYYY-MM-DD');
        if(!req.body.user_id){
            res.send({
                status: 1,
                message: 'user not existed'
            })
        }
        else{
            team_member
                .findAll({
                    where: {
                        user_id: req.body.user_id
                    },
                    attributes: ['id', 'team_id']
                })
                .then(values => {
                    if(values.length === 0){
                        res.send({
                            status: 1,
                            data: {
                                member_task: [],
                                doneTasksNumber: doneTasks,
                                taskNumber: taskNumber,
                                ongoingTasksNumber: ongoingTasks,
                                blockedTasksNumber: blockedTasks
                            }
                        })
                    }
                    else{
                        values.forEach(item => {
                            nIdTeams.push(item.dataValues.team_id)
                        })
                    }
                })
                .then(() => {
                    if(nIdTeams.length !== 0){
                        team_member
                            .findAll({
                                where: {
                                    team_id: {
                                        [Sequelize.Op.in]: nIdTeams
                                    }
                                },
                                attributes: ['id']
                            })
                            .then(values => {
                                values.forEach(item => {
                                    idTeams.push(item.id)
                                })
                            })
                            .then(() => {
                                team_member_task
                                    .findAll({
                                        where: {
                                            [Sequelize.Op.and]: [
                                                {
                                                    team_member_id: {
                                                        [Sequelize.Op.in]: idTeams
                                                    }
                                                },
                                                {
                                                    archived: false
                                                },
                                                {
                                                    worked: true
                                                }
                                            ]
                                        }
                                    })
                                    .then(val => {
                                        if(val.length === 0){
                                            res.send({
                                                status: 1,
                                                data: {
                                                    member_task: [],
                                                    doneTasksNumber: doneTasks,
                                                    taskNumber: taskNumber,
                                                    ongoingTasksNumber: ongoingTasks,
                                                    blockedTasksNumber: blockedTasks
                                                }
                                            })
                                        }
                                        else{
                                            val.forEach(item => {
                                                idTeamTasks.push(item.dataValues.task_id)
                                            })
                                        }
                                    })
                                    .then(() => {
                                        if(idTeamTasks.length !== 0){
                                            task
                                                .count({
                                                    where: {
                                                        [Sequelize.Op.and]: [
                                                            {
                                                                status: 'done'
                                                            },
                                                            {
                                                                endDate: {
                                                                    [Sequelize.Op.between]: [now, nextDay]
                                                                }
                                                            },
                                                            {
                                                                id: {
                                                                    [Sequelize.Op.in]: idTeamTasks
                                                                }
                                                            }
                                                        ]
                                                    }
                                                })
                                                .then(done_tasks => {
                                                    doneTasks += done_tasks
                                                })
                                                .then(() => {
                                                    task
                                                        .count({
                                                            where: {
                                                                [Sequelize.Op.and]: [
                                                                    {
                                                                        status: 'ongoing'
                                                                    },
                                                                    {
                                                                        endDate: {
                                                                            [Sequelize.Op.between]: [now, nextDay]
                                                                        }
                                                                    },
                                                                    {
                                                                        id: {
                                                                            [Sequelize.Op.in]: idTeamTasks
                                                                        }
                                                                    }
                                                                ]
                                                            }
                                                        })
                                                        .then(ongoing_tasks => {
                                                            ongoingTasks += ongoing_tasks
                                                        })
                                                        .then(() => {
                                                            task
                                                                .count({
                                                                    where: {
                                                                        [Sequelize.Op.and]: [
                                                                            {
                                                                                status: 'locked'
                                                                            },
                                                                            {
                                                                                endDate: {
                                                                                    [Sequelize.Op.between]: [now, nextDay]
                                                                                }
                                                                            },
                                                                            {
                                                                                id: {
                                                                                    [Sequelize.Op.in]: idTeamTasks
                                                                                }
                                                                            }
                                                                        ]
                                                                    }
                                                                })
                                                                .then(locked_tasks => {
                                                                    blockedTasks += locked_tasks
                                                                })
                                                                .then(() => {
                                                                    task
                                                                        .count({
                                                                            where: {
                                                                                [Sequelize.Op.and]: [
                                                                                    {
                                                                                        endDate: {
                                                                                            [Sequelize.Op.between]: [now, nextDay]
                                                                                        }
                                                                                    },
                                                                                    {
                                                                                        id: {
                                                                                            [Sequelize.Op.in]: idTeamTasks
                                                                                        }
                                                                                    }
                                                                                ]
                                                                            }
                                                                        })
                                                                        .then(number_tasks => {
                                                                            taskNumber += number_tasks
                                                                        })
                                                                        .then(() => {
                                                                            task
                                                                                .findAll({
                                                                                    where: {
                                                                                        [Sequelize.Op.and]: [
                                                                                            {
                                                                                                endDate: {
                                                                                                    [Sequelize.Op.between]: [now, nextDay]
                                                                                                }
                                                                                            },
                                                                                            {
                                                                                                id: {
                                                                                                    [Sequelize.Op.in]: idTeamTasks
                                                                                                }
                                                                                            }
                                                                                        ]
                                                                                    }
                                                                                })
                                                                                .then(tasks => {
                                                                                    if(tasks.length == 0){
                                                                                        res.send({
                                                                                            status: 1,
                                                                                            data: {
                                                                                                member_task: [],
                                                                                                doneTasksNumber: doneTasks,
                                                                                                taskNumber: taskNumber,
                                                                                                ongoingTasksNumber: ongoingTasks,
                                                                                                blockedTasksNumber: blockedTasks
                                                                                            }
                                                                                        })
                                                                                    }
                                                                                    else {
                                                                                        tasks.forEach(item => {
                                                                                            idTasks.push(item.dataValues.id)
                                                                                        })
                                                                                    }
                                                                                })
                                                                                .then(() => {
                                                                                    if(idTasks.length !== 0){
                                                                                        team_member_task
                                                                                            .findAll({
                                                                                                include: {
                                                                                                    model: task,
                                                                                                    as: 'Task'
                                                                                                },
                                                                                                where: {
                                                                                                    task_id: {
                                                                                                        [Sequelize.Op.in]: idTasks
                                                                                                    }
                                                                                                }
                                                                                            })
                                                                                            .then(values => {
                                                                                                getTasksUsers(values, _tasks => {
                                                                                                    res.send({
                                                                                                        status: 1,
                                                                                                        data: {
                                                                                                            member_task: _tasks,
                                                                                                            doneTasksNumber: doneTasks,
                                                                                                            taskNumber: taskNumber,
                                                                                                            ongoingTasksNumber: ongoingTasks,
                                                                                                            blockedTasksNumber: blockedTasks
                                                                                                        }
                                                                                                    })
                                                                                                })
                                                                                            })
                                                                                            .catch(error => {
                                                                                                res.send({
                                                                                                    status: 0,
                                                                                                    error: error.message
                                                                                                })

                                                                                            })
                                                                                    }
                                                                                })
                                                                                .catch(error => {
                                                                                    res.send({
                                                                                        status: 0,
                                                                                        error: error.message
                                                                                    })
                                                                                })
                                                                        })
                                                                        .catch(error => {
                                                                            res.send({
                                                                                status: 0,
                                                                                error: error.message
                                                                            })
                                                                        })

                                                                })
                                                                .catch(error => {
                                                                    res.send({
                                                                        status: 0,
                                                                        error: error.message
                                                                    })
                                                                })
                                                        })
                                                        .catch(error => {
                                                            res.send({
                                                                status: 0,
                                                                error: error.message
                                                            })
                                                        })
                                                })
                                                .catch(error => {
                                                    res.send({
                                                        status: 0,
                                                        error: error.message
                                                    })
                                                })
                                        }
                                    })
                                    .catch(error => {
                                        res.send({
                                            status: 0,
                                            error: error.message
                                        })
                                    })
                            })
                            .catch(error => {
                                res.send({
                                    status: 0,
                                    error: error.message
                                })
                            })
                    }
                })
                .catch(error => {
                    res.send({
                        status: 0,
                        error: error.message
                    })
                })
        }
    },

    listProfessions(req, res){
        profession
            .findAll({
                attributes: ['id', 'name']
            })
            .then(_professions => {
                let roles = [
                    {
                        id: 1,
                        name: 'Team member'
                    },
                    {
                        id: 2,
                        name: 'Team leader'
                    }
                ]
                res.send({
                    status: 1,
                    data: {
                        professions: _professions,
                        roles: roles
                    }
                })
            })
            .catch(error => {
                res.send({
                    status: 0,
                    error: error.message
                })
            })
    }
}

getTasksUsers = (tasks, callback, i=0) => {
    if(tasks.length > i){
        team_member
            .findOne({
                include: {
                    model: user,
                    as: 'User'
                },
                where: {
                    id: tasks[i].dataValues.id
                }
            })
            .then(value => {
                tasks[i].dataValues.user = value.dataValues
                getTasksUsers(tasks, callback, i+1)
            })
    }
    else{
        callback(tasks)
    }
}