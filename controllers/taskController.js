var user = require('../models').Users;
var task = require('../models').Task;
var team = require('../models').Team;
var team_member_task = require('../models').Team_member_task;
var team_member = require('../models').Team_member;
var Sequelize = require('sequelize');
var moment = require('moment');

module.exports = {
    list(req, res){
        if(!req.body.user_id){
            res.send({
                status: 1,
                message: 'User not existed'
            })
        }
        else{
            var idTasks = new Array();
            var idTeams = new Array();
            var startDate = moment(new Date()).format('YYYY-MM-DD');
            var startDayOfWeek = moment(startDate).day();
            if(startDayOfWeek === 0) {
                var endDate = startDate;
            }
            else{
                var numberDays = (6 - startDayOfWeek) + 1
                var endDate = (moment(startDate).add(numberDays, 'days')).format('YYYY-MM-DD');
            }

            team_member
                .findAll({
                    where: {
                        user_id: req.body.user_id
                    },
                    attributes: ['id']
                })
                .then(_teams => {
                    if(_teams.length === 0){
                        res.send({
                            status: 1,
                            data: {
                                member_task: [],
                            }
                        })
                    }
                    else{
                        _teams.forEach(item => {
                            idTeams.push(item.dataValues.id)
                        })
                    }
                })
                .then(() => {
                    if(idTeams.length !== 0){
                        task
                            .findAll({
                                where: {
                                    endDate: {
                                        [Sequelize.Op.between]: [startDate, endDate]
                                    }
                                }
                            })
                            .then(tasks => {
                                if(tasks.length == 0){
                                    res.send({
                                        status: 1,
                                        data: {
                                            member_task: [],
                                        }
                                    })
                                }
                                else{
                                    tasks.forEach(item => {
                                        idTasks.push(item.dataValues.id)
                                    })
                                }
                            })
                            .then(() => {
                                if(idTasks.length !== 0){
                                    team_member_task
                                        .findAll({
                                            include: {
                                                model: task,
                                                as: 'Task'
                                            },
                                            where: {
                                                task_id: {
                                                    [Sequelize.Op.in]: idTasks
                                                },
                                                team_member_id: {
                                                    [Sequelize.Op.in]: idTeams
                                                }
                                            },
                                        })
                                        .then(values => {
                                            getTasksUsers(values, _tasks => {
                                                res.send({
                                                    status: 1,
                                                    data: {
                                                        member_task: _tasks,
                                                    }
                                                })
                                            })
                                        })
                                        .catch(error => {
                                            res.send({
                                                status: 0,
                                                error: error.message
                                            })

                                        })
                                }
                            })
                            .catch(error => {
                                res.send({
                                    status: 0,
                                    error: error.message
                                })
                            })

                    }
                })
                .catch(error => {
                    res.send({
                        status: 0,
                        error: error.message
                    })

                })
        }
    },

    edit(req, res){
        if(!req.body.task_id){
            res.send({
                status: 1,
                message: 'task not existed'
            })
        }
        else{
            if(!req.body.task_member_id){
                task
                    .findByPk(req.body.task.id)
                    .then(_task => {
                        _task
                            .update({
                                name: req.body.task.name,
                                endDate: req.body.task.endDate,
                                priority: req.body.task.priority,
                                status: req.body.task.status
                            })
                            .then(() => {
                                res.send({
                                    status: 1,
                                    data: {
                                        task_id: _task.dataValues.id
                                    }
                                })
                            })
                            .catch(error => {
                                res.send({
                                    status: 0,
                                    error: error.message
                                })
                            })
                    })
                    .catch(error => {
                        res.send({
                            status: 0,
                            error: error.message
                        })
                    })
            }
            else{
                task
                    .findByPk(req.body.task.id)
                    .then(_task => {
                        _task
                            .update({
                                name: req.body.task.name,
                                endDate: req.body.task.endDate,
                                priority: req.body.task.priority,
                                status: req.body.task.status
                            })
                            .then(() => {
                                team_member
                                    .findOne({
                                        where: {
                                            [Sequelize.Op.and]: [
                                                {
                                                    team_id: req.body.team_id,
                                                },
                                                {
                                                    user_id: req.body.user_id
                                                }
                                            ]
                                        }
                                    })
                                    .then(_value => {
                                        if(_value){
                                            team_member_task
                                                .create({
                                                    team_member_id: _value.dataValues.id,
                                                    task_id: _task.dataValues.id
                                                })
                                                .then(() => {
                                                    res.send({
                                                        status: 1
                                                    })
                                                })
                                                .catch(error => {
                                                    res.send({
                                                        status: 0,
                                                        error: error.message
                                                    })
                                                })
                                        }
                                        else{
                                            team_member
                                                .create({
                                                    team_id: req.body.team_id,
                                                    user_id: req.body.user_id
                                                })
                                                .then(value => {
                                                    team_member_task
                                                        .create({
                                                            team_member_id: value.dataValues.id,
                                                            task_id: _task.dataValues.id
                                                        })
                                                        .then(() => {
                                                            res.send({
                                                                status: 1
                                                            })
                                                        })
                                                        .catch(error => {
                                                            res.send({
                                                                status: 0,
                                                                error: error.message
                                                            })
                                                        })
                                                })
                                                .catch(error => {
                                                    res.send({
                                                        status: 0,
                                                        error: error.message
                                                    })
                                                })
                                        }
                                    })
                                    .catch(error => {
                                        res.send({
                                            status: 0,
                                            error: error.message
                                        })
                                    })
                            })
                            .catch(error => {
                                res.send({
                                    status: 0,
                                    error: error.message
                                })
                            })
                    })
                    .catch(error => {
                        res.send({
                            status: 0,
                            error: error.message
                        })
                    })

            }
        }
    },

    create(req, res){
        team_member
            .findOne({
                where: {
                    team_id: req.body.team_id
                },
                attributes:['id', 'type', 'team_id', 'user_id']
            })
            .then(_team => {
                if(_team.dataValues.type !== 'admin'){
                    res.send({
                        status: 1,
                        message: 'user can\'t create task'
                    })
                }
                else{
                    task
                        .create({
                            name: req.body.task.name,
                            endDate: moment(req.body.task.endDate).format("YYYY-MM-DD"),
                            startDate: moment(new Date()).format("YYYY/MM/DD"),
                            priority: req.body.task.priority,
                            status: 'pending',
                            description: req.body.task.description
                        })
                        .then(_task => {
                            if(!req.body.team_id){
                                res.send({
                                    status: 1,
                                    message: "task created successful"
                                })
                            }
                            else{
                                team_member_task
                                    .create({
                                        team_member_id: _team.dataValues.id,
                                        task_id: _task.dataValues.id,
                                        worked: false
                                    })
                                    .then(() => {
                                        res.send({
                                            status: 1,
                                            message: "task created successful"
                                        })
                                    })
                                    .catch(error => {
                                        res.send({
                                            status: 0,
                                            error: error.message
                                        })
                                    })
                            }
                        })
                        .catch(error => {
                            res.send({
                                status: 0,
                                error: error.message
                            })
                        })

                }
            })
    },

    addWorker(req, res){
        if(!req.body.user_id){
            res.send({
                status: 1,
                message: 'User not existed'
            })
        }
        else{
            team_member
                .findOne({
                    where: {
                        [Sequelize.Op.and]: [
                            {
                                user_id:  req.body.user_id
                            },
                            {
                                team_id: req.body.team_id
                            }
                        ]
                    },
                    attributes: ['id']
                })
                .then(_team => {
                    team_member_task
                        .create({
                            team_member_id: _team.dataValues.id,
                            task_id: req.body.task_id,
                            worked: true
                        })
                        .then(() => {
                            task
                                .findOne({
                                    where: {
                                        id: req.body.task_id
                                    }
                                })
                                .then(_task => {
                                    _task
                                        .update({
                                            status: 'ongoing'
                                        })
                                        .then(() => {
                                            res.send({
                                                status: 1,
                                                message: "worker added successful"
                                            })
                                        })
                                        .catch(error => {
                                            res.send({
                                                status: 0,
                                                error: error.message
                                            })
                                        })
                                })
                                .catch(error => {
                                    res.send({
                                        status: 0,
                                        error: error.message
                                    })
                                })
                        })
                        .catch(error => {
                            res.send({
                                status: 0,
                                error: error.message
                            })
                        })
                })
                .catch(error => {
                    res.send({
                        status: 0,
                        error: error.message
                    })
                })
        }
    },

    getTaskInformation(req, res){
        team_member
            .findAll({
                include: {
                    model: team,
                    as: 'Team'
                },
                where: {
                    user_id: req.body.user_id
                }
            })
            .then(values => {
                if(values.length !== 0){
                    res.send({
                        status: 1,
                        data: {
                            teams: values
                        }
                    })
                }
                else{
                    team_member
                        .findAll({
                            include: {
                                model: team,
                                as: 'Team'
                            }
                        })
                        .then(_teams => {
                            res.send({
                                status: 1,
                                data: {
                                    teams: _teams
                                }
                            })
                        })
                        .catch(error => {
                            res.send({
                                status: 0,
                                error: error.message
                            })
                        })
                }
            })
            .catch(error => {
                res.send({
                    status: 0,
                    error: error.message
                })
            })
    }
}

getTasksUsers = (tasks, callback, i=0) => {
    if(tasks.length > i){
        team_member
            .findOne({
                include: {
                    model: user,
                    as: 'User'
                },
                where: {
                    id: tasks[i].dataValues.team_member_id
                }
            })
            .then(value => {
                team_member_task
                    .findAll({
                        where: {
                            task_id: tasks[i].dataValues.task_id
                        }
                    })
                    .then(values => {
                        if(values.length === 1){
                            tasks[i].dataValues.type = value.dataValues.type
                            getTasksUsers(tasks, callback, i+1)
                        }
                        else{
                            team_member
                                .findOne({
                                    include: {
                                        model: user,
                                        as: 'User'
                                    },
                                    where: {
                                        id: values[1].dataValues.team_member_id
                                    }
                                })
                                .then(user => {
                                    tasks[i].dataValues.User = user.dataValues.User
                                    tasks[i].dataValues.type = value.dataValues.type
                                    getTasksUsers(tasks, callback, i+1)
                                })
                        }
                    })
            })

    }
    else{
        callback(tasks)
    }
}