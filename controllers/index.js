const authController = require('./authController');
const homeController = require('./homeController');
const taskController = require('./taskController');
const userController = require('./userController');
const teamController = require('./teamController');

module.exports = {
    authController,
    homeController,
    taskController,
    userController,
    teamController
};