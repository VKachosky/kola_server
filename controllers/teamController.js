var user = require('../models').Users;
var task = require('../models').Task;
var team = require('../models').Team;
var team_member_task = require('../models').Team_member_task;
var team_member = require('../models').Team_member;
var Sequelize = require('sequelize');

module.exports = {
    list(req,res){
        if(!req.body.user_id){
            res.send({
                status: 1,
                message: 'User not existed'
            })
        }
        else{
            team_member
                .findAll({
                    include: {
                        model: team,
                        as: 'Team'
                    },
                    where: {
                      user_id: req.body.user_id
                    },
                    attributes: ['id', 'team_id', 'user_id', 'type'],
                    through: {attributes:['Team'], as : 'Team'}
                })
                .then(values => {
                    res.send({
                        status: 1,
                        data: {
                            teams: values
                        }
                    })
                })
                .catch(error => {
                    res.send({
                        status: 0,
                        error: error.message
                    })
                })

        }
    },

    create(req, res){
        if(!req.body.user_id){
            res.send({
                status: 1,
                message: 'User not existed'
            })
        }
        else{
            team
                .create({
                    name: req.body.team.name,
                    role: req.body.team.role,
                    context: req.body.team.context,
                    description: req.body.team.description
                })
                .then(_team => {
                    team_member
                        .create({
                            team_id: _team.dataValues.id,
                            user_id: req.body.user_id,
                            type: 'admin'
                        })
                        .then(() => {
                            res.send({
                                status: 1,
                                message: 'team created successful'
                            })
                        })
                        .catch(error => {
                            res.send({
                                status: 0,
                                error: error.message
                            })
                        })
                })
                .catch(error => {
                    res.send({
                        status: 0,
                        error: error.message
                    })
                })
        }
    },

    addMember(req, res){
        if(!req.body.user_id){
            res.send({
                status: 1,
                message: 'User not existed'
            })
        }
        else{
            team_member
                .findOne({
                    where: {
                        [Sequelize.Op.and]: [
                            {
                                team_id: req.body.team_id
                            },
                            {
                                user_id: req.body.user_id
                            }
                        ]
                    }
                })
                .then(value => {
                    if(value){
                        res.send({
                            status: 1,
                            message: 'user is already in team'
                        })
                    }
                    else{
                        team_member
                            .create({
                                team_id: req.body.team_id,
                                user_id: req.body.user_id,
                                type: (req.body.isAdmin)? 'admin': 'member'
                            })
                            .then(() => {
                                res.send({
                                    status: 1,
                                    message: 'member added successful'
                                })
                            })
                            .catch(error => {
                                res.send({
                                    status: 0,
                                    error: error.message
                                })
                            })
                    }
                })
                .catch(error => {
                    res.send({
                        status: 0,
                        error: error.message
                    })
                })
        }
    },

    getListMember(req,res){
        if(!req.body.user_id){
            res.send({
                status: 1,
                message: 'User not existed'
            })
        }
        else{
            team_member
                .findAll({
                    include: [
                        {
                            model: team,
                            as: 'Team'
                        },
                        {
                            model: user,
                            as: 'User'
                        }
                    ],
                    /*where: {
                        [Sequelize.Op.and]: [
                            {
                                team_id: req.body.team_id
                            },
                            {
                                user_id: {
                                    [Sequelize.Op.ne]: req.body.user_id
                                }
                            }
                        ]
                    }*/
                    where: {
                        team_id: req.body.team_id
                    }
                })
                .then(_users => {
                    res.send({
                        status: 1,
                        data: {
                            team_users: _users
                        }
                    })
                })
                .catch(error => {
                    res.send({
                        status: 0,
                        error: error.message
                    })
                })
        }
    }
}