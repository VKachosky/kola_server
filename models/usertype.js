'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserType = sequelize.define('UserType', {
    name: DataTypes.STRING
  }, {
      timestamps: true,
      freezeTableName: true,
      tableName: 'userTypes'
  });
  UserType.associate = function(models) {
    UserType.hasMany(models.Users, {
        foreignKey: 'userType_id',
        as: 'users'
    })
  };
  return UserType;
};