'use strict';

const Team_member = require('./team_member');
const Task = require('./task');

module.exports = (sequelize, DataTypes) => {
  const Team_member_task = sequelize.define('Team_member_task', {
      team_member_id: {
          type: DataTypes.INTEGER,
          references: {
              model: Team_member,
              key: 'id'
          }
      },
      task_id: {
          type: DataTypes.INTEGER,
          references: {
              model: Task,
              key: 'id'
          }
      },
      archived: {
          type: DataTypes.BOOLEAN,
          defaultValue: false
      },
      worked: {
          type: DataTypes.BOOLEAN,
          defaultValue: false
      }
  }, {
      timestamps: true,
      freezeTableName: true,
      tableName: 'team_member_tasks'
  });
  Team_member_task.associate = function(models) {
      /*Team_member_task.belongsTo(models.Team_member, {
          foreignKey: 'team_member_id',
          targetKey: 'id'
      });*/

      Team_member_task.belongsTo(models.Task, {
          foreignKey: 'task_id',
          targetKey: 'id'
      })
  };
  return Team_member_task;
};