'use strict';

var bcrypt = require('bcryptjs');

const UserType = require('./usertype');
const Profession = require('./profession');

module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {
      name: DataTypes.STRING,
      email: {
          type: DataTypes.STRING,
          allowNull: false
      },
      password: {
          type: DataTypes.STRING,
          allowNull: false
      },
      userType_id: {
          type: DataTypes.INTEGER,
          references: {
              model: UserType,
              key: 'id'
          }
      },
      profession_id: {
          type: DataTypes.INTEGER,
          references: {
              model: Profession,
              key: 'id'
          }
      }

  }, {
      timestamps: true,
      freezeTableName: true,
      tableName: 'users'
  });

    Users.beforeSave((user, options) => {
        if(user.changed('password')){
            user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null);
        }
    });

    Users.beforeUpdate((user, options) => {
        if(user.changed('password')){
            user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10), null);
        }
    });

    Users.prototype.comparePassword = function(password, cb){
        bcrypt.compare(password, this.password, function (err, isMatch) {
            if(err) {
                return cb(err);
            }
            cb(null, isMatch);
        });
    };


  Users.associate = function(models) {
      Users.belongsTo(models.UserType, {
          foreignKey: 'userType_id',
          targetKey: 'id'
      });

      Users.belongsTo(models.Profession, {
          foreignKey: 'profession_id',
          targetKey: 'id'
      });

      Users.belongsToMany(models.Team, {
          through: 'Team_member',
          as: 'teams',
          foreignKey: 'user_id'
      })
  };
  return Users;
};