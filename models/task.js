'use strict';
module.exports = (sequelize, DataTypes) => {
  const Task = sequelize.define('Task', {
      name: DataTypes.STRING,
      startDate: DataTypes.DATE,
      endDate: DataTypes.DATE,
      priority: DataTypes.ENUM('high', 'medium', 'low'),
      status: DataTypes.ENUM('done', 'ongoing', 'pending', 'locked'),
      description: DataTypes.STRING

  }, {
      timestamps: true,
      freezeTableName: true,
      tableName: 'tasks'
  });

  Task.associate = function(models) {
      Task.belongsToMany(models.Team_member, {
          through: 'Team_member_task',
          as: 'team_members',
          foreignKey: 'task_id'
      });

  };
  return Task;
};