'use strict';
module.exports = (sequelize, DataTypes) => {
  const Profession = sequelize.define('Profession', {
    name: DataTypes.STRING
  }, {
      timestamps: true,
      freezeTableName: true,
      tableName: 'professions'
  });
  Profession.associate = function(models) {
      Profession.hasMany(models.Users, {
          foreignKey: 'profession_id',
          as: 'users'
      })
  };
  return Profession;
};