'use strict';

const Team = require('./team');
const Users = require ('./users');
module.exports = (sequelize, DataTypes) => {
  const Team_member = sequelize.define('Team_member', {
      type: DataTypes.ENUM('admin', 'member'),
      team_id: {
          type: DataTypes.INTEGER,
          references: {
              model: Team,
              key: 'id'
          }
      },
      user_id: {
          type: DataTypes.INTEGER,
          references: {
              model: Users,
              key: 'id'
          }
      }
  }, {
      timestamps: true,
      freezeTableName: true,
      tableName: 'team_members'
  });
  Team_member.associate = function(models) {
      Team_member.belongsTo(models.Users, {
          foreignKey: 'user_id',
          targetKey: 'id'
      });

      Team_member.belongsTo(models.Team, {
          foreignKey: 'team_id',
          targetKey: 'id'
      });

      Team_member.belongsToMany(models.Task, {
          through: 'Team_member_task',
          as: 'tasks',
          foreignKey: 'team_member_id'
      })
  };
  return Team_member;
};