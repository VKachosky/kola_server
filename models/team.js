'use strict';
module.exports = (sequelize, DataTypes) => {
  const Team = sequelize.define('Team', {
    name: DataTypes.STRING,
    role: DataTypes.STRING,
    context: DataTypes.STRING,
    description: DataTypes.STRING
  }, {
      timestamps: true,
      freezeTableName: true,
      tableName: 'teams'
  });
  Team.associate = function(models) {
      Team.belongsToMany(models.Users, {
          through: 'Team_member',
          as: 'users',
          foreignKey: 'team_id'
      })
  };
  return Team;
};