var express = require('express');
var router = express.Router();
var domaine = 'api/'

var authController = require('../controllers').authController;
var homeController = require ('../controllers').homeController;
var taskController = require('../controllers').taskController;
var userController = require('../controllers').userController;
var teamController = require('../controllers').teamController;


/* GET home page. */
router.post('/' + domaine + 'auth/register', authController.register);
router.post('/' + domaine + 'auth/login', authController.login);

router.post('/' + domaine + 'home', homeController.index);
router.get('/' + domaine + 'professions', homeController.listProfessions);

router.post('/' + domaine + 'tasks', taskController.list);
router.post('/' + domaine + 'task/edit', taskController.edit); // à revoir pour choisir à quelle équipe la tâche est attribuée
router.post('/' + domaine + 'task/create', taskController.create);
router.post('/' + domaine + 'task', taskController.getTaskInformation);
router.post('/' + domaine + 'task/add_worker', taskController.addWorker);

router.post('/' + domaine + 'users', userController.list);
router.post('/' + domaine + 'list_users', userController.list_users);
router.post('/' + domaine + 'user/', userController.getUserInformation);
router.post('/' + domaine + 'user/edit', userController.editProfil);


router.post('/' + domaine + 'teams', teamController.list);
router.post('/' + domaine + 'team/create', teamController.create);// seulement l'admin (chef de projet) qui peut créer
router.post('/' + domaine + 'team/add_member', teamController.addMember); // c'est l'admin d'une équipe ou le chef de projet qui peut ajouter un membre (étant admin ou pas).il y'a qu'un seul admin dans une équipe (en dehors du chef de projet)
router.post('/' + domaine + 'team/', teamController.getListMember); //afficher les informations nécessaires, retirer les mots de passe et autres. La liste des membres d'une équipe exclu le chef de projet



module.exports = router;
